function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementSporociloTekst(sporocilo){
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    var smeski = [":)", ";)", "(y)", ":*", ":("];
    var nadomestki = ["<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png\">",
                      "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png\">",
                      "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png\">",
                      "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png\">",
                      "<img src=\"https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png\">"];
    
    
      var temp="";
      for(var j=0; j<smeski.length; j++){
        var mesto=sporocilo.indexOf(smeski[j]);
        while(mesto!=-1){
          for(var i=0; i<sporocilo.length; i++){
            if(i==mesto){
              temp+=nadomestki[j];
            }
            else if(mesto==i-1){
            
            }
            else if(mesto==i-2 && j==2){
          
            }
            else{
              temp+=sporocilo.charAt(i);
            }
          }
          sporocilo=temp;
          temp="";
          mesto=sporocilo.indexOf(smeski[j]);
        }
      }
     
    
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    //$('#sporocila').append(divElementSporociloTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  var vzdevek;
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      vzdevek=rezultat.vzdevek;
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#kanal').text(rezultat.vzdevek+" @ "+rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.besedilo+" @ "+rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki){
    console.log("Prestrezeno");
    $('#seznam-uporabnikov').empty();
    for(var k=0; k<uporabniki.length; k++){
      console.log("uporabniki[k]"+uporabniki[k]);
    }
    
    for(var l=0; l<uporabniki.length; l++) {
      var uporabnik = uporabniki[l];
      console.log(uporabnik);
      if (uporabnik != '') {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabnik));
      }
    }
    
  })

  socket.on('zasebno', function(message){
    console.log("message sprejet");
    console.log("this: "+this.socket.id+"uporabnik: "+message.uporabnik);
    console.log(message.besedilo);
    if(message.uporabnik==vzdevek){
      $('#sporocila').append(divElementEnostavniTekst(message.posiljatelj+" (zasebno): "+message.besedilo));
    }
  });
  
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});